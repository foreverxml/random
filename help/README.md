# Random
Random aims to make randomization easy. But, just in case it is not easy, these help pages will make using Random very easy.
#### This might take a really long time to load. Apologies.
## Contents
* [How to pick a number](#how-to-pick-a-number)
* [How to pick an item in Roulette](#how-to-pick-an-item-in-roulette)
    + [Why the separator?](#why-the-separator)
* [Switching between the views](#switching-between-the-views)
* [Converting from Number to Roulette](#converting-from-number-to-roulette)
* [Spinning and removing in Roulette](#spinning-and-removing-in-roulette)
## How to pick a number
Random will launch in the *Number* view, denoted by a keypad in the view switcher. To generate a number, you need to have a range of numbers; for this example, that range is from 15 to 30. You put your lower number in the **first box** and the higher number in the **second box**. For our exaple, 15 goes in the first box and 30 goes into the second. Then, click the *Generate* button to generate a number, or press `Ctrl` + `G` on your keyboard. You can see how it's done below (click on the image for a moving version):

![Generating a number in Random](./screenshots/number.gif)
## How to pick an item in Roulette
You will have to [switch views](#switching-between-the-views) to use the *Roulette* view. To roll the *Roulette* wheel, you have to have a list, and a separator. Let's consider the list `A, B, C, D`. That list would be the list, and the separator would be `, ` with that extra space. Paste or write your list in the long box, and in the short box write in your separator. Then, click the *Pick* button, or press `Ctrl` + `G` to spin the *Roulette* wheel. You can see how it's done below (click on the image for a moving version):

![Spinning the Roulette wheel in Random](./screenshots/roulette.gif)
### Why the separator?
Due to how *Random* works, you need a separator to separate the contents of the list. *Random* cannot automatically detect your separator.
## How to flip a coin
You will have to [switch views](#switching-between-the-views) to use the *Coin* view. To flip a coin in Random, you simply press the "Flip" button or `Ctrl` + `G` , and it will flip the coin and give you a result of heads or tails. You can see how it's done below (click on the image for a moving version):

![Flipping a coin in Random](./screenshots/coin.gif)
## Switching between the views
In *Random*, there are 3 ways to switch between the views. Here are those 3 ways.
### 1: With the Mouse
Click on the views in the View switcher.

![Method 1](./screenshots/switch-mouse.gif)
### 2: With Tab and Enter
You can use `Tab` or the arrow keys to switch between focus on the view buttons and press `Enter` to select that view.

![Method 2](./screenshots/switch-tab-enter.gif)
### 3: Using a keyboard shortcut
You can use `Ctrl` + `Tab` to switch between views.

![Method 3](./screenshots/switch-ctrl-tab.gif)
## Converting from Number to Roulette
In *Random* you can convert your *Number* generation to a *Roulette* list. First, you open the *Random* app main menu, which you can do by clicking it or pressing `F10`, and selecting *Number to Roulette*. Alternatively, you can press `Ctrl` + `Shift` + `R`. It will first switch to the *Number* view, so you can enter your number range as shown in the *[Number](#how-to-pick-a-number)* help sheet. Then, you press the button or shortcut again, and it will convert your range of numbers to a *Roulette* list you can use in *Roulette*. You can see how it's done below (click on the image for a moving version):

![Number to Roulette demo](./screenshots/number-roulette.gif)
## Spinning and removing in Roulette
You can spin the *Roulette* wheel and delete the spun item in *Random*. To do it, follow the *[Roulette](#how-to-pick-an-item-in-roulette)* help sheet, but don't click the *Spin* button. Instead, open the *Random* app main menu by clicking the menu button or pressing `F10`. Then, select *Delete Picked Item*, or `Ctrl` + `D` and it will spin the Roulette wheel. It's normal for it to not delete anything on the first run! When you do it again, it will delete the spun item, and generate again. You can see how it's done below (click on the image for a moving version):

![Spinning and removing in the Roulette view](./screenshots/pick-remove.gif)